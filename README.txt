***********
* README: *
***********

DESCRIPTION:
------------
This module provides the ability to add extra View Modes for entity types such
as nodes, users, and comments.

It is a simplified version of Entity View Modes: https://www.drupal.org/project/entity_view_mode

It is a "Minimalist" module. It has no UI for module settings. See Configuration
section below.


INSTALLATION:
-------------
1. Place the entire m_entity_view_modes directory into your Drupal sites/all/modules/
   directory.

2. Enable the Minimalist module by navigating to:

     administer > modules


CONFIGURATION:
---------
This module reads it's setting from a Drupal variable with the key m_entity_view_modes.
The variable should be set like this:
array(
 'node' => array( // Entity type machine name key
   'featured' => array( // View mode machine name key
     'label' => 'Featured',
     'custom settings' => FALSE,
 ),
)

* Suggested methods for setting variable:
1. Set directly in settings.php via $conf variable or
2. Include the following in settings.php to include a configuration file
/*
 * Conf file
 */
if (file_exists(dirname(__FILE__) . '/site_conf.php')) {
  include dirname(__FILE__) . '/site_conf.php';
}

In site_conf.php set variable via $conf for this module and other "Minimalist" modules.
